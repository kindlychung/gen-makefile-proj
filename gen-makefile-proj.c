#define _GNU_SOURCE
#include <regex.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>  // strcmp
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

// memory-safe asprintf, implemented as a macro
#define Sasprintf(write_to, ...)                         \
    ({                                                   \
        char *tmp_string_for_extend = (write_to);        \
        int result = asprintf(&(write_to), __VA_ARGS__); \
        free(tmp_string_for_extend);                     \
        result;                                          \
    })

#define MakeDir(_path)                                                 \
    {                                                                  \
        __typeof__(_path) path = (_path);                              \
        if (mkdir(path, 0700) == -1) {                                 \
            fprintf(stderr, "Failed to create directory: %s\n", path); \
            return 1;                                                  \
        }                                                              \
    }

#define MakeDir2(parent, sub_dir)                                      \
    {                                                                  \
        __typeof__(parent) _parent = (parent);                         \
        __typeof__(sub_dir) _sub_dir = (sub_dir);                      \
        char *path = NULL;                                             \
        if (Sasprintf(path, "%s/%s", _parent, _sub_dir) == -1) {       \
            fprintf(stderr, "Failed to allocate string.\n");           \
            return 1;                                                  \
        }                                                              \
        if (mkdir(path, 0700) == -1) {                                 \
            fprintf(stderr, "Failed to create directory: %s\n", path); \
            return 1;                                                  \
        }                                                              \
    }

#define WriteFile(parent, filename, data)                         \
    {                                                             \
        __typeof__(parent) _parent = (parent);                    \
        __typeof__(filename) _filename = (filename);              \
        __typeof__(data) _data = (data);                          \
        char *path = NULL;                                        \
        if (Sasprintf(path, "%s/%s", _parent, _filename) == -1) { \
            fprintf(stderr, "Failed to allocate string.\n");      \
            return 1;                                             \
        }                                                         \
        write_to_file(path, _data);                               \
        free(path);                                               \
    }

#define WriteFileWitExtension(parent, ext, data)                         \
    {                                                                    \
        __typeof__(parent) _parent = (parent);                           \
        __typeof__(ext) _ext = (ext);                                    \
        __typeof__(data) _data = (data);                                 \
        char *path = NULL;                                               \
        if (Sasprintf(path, "%s/%s.%s", _parent, _parent, _ext) == -1) { \
            fprintf(stderr, "Failed to allocate string.\n");             \
            return 1;                                                    \
        }                                                                \
        write_to_file(path, _data);                                      \
        free(path);                                                      \
    }

struct stat st = {0};

char const *help_msg =
    "\ngen-makefile-proj: Create a template makefile project.\n"
    "(c) Kaiyin Zhong 2018\n\n"
    "Usage: \n"
    "gen-makefile-proj <project_name>\n"
    "gen-makefile-proj -h \n"
    "gen-makefile-proj --help \n";

regex_t regex;
int regex_error;
char const *regex_string = "^[a-zA-Z0-9]+[a-zA-Z0-9_-]*$";

void write_to_file(const char *filepath, const char *data) {
    FILE *fp = fopen(filepath, "ab");
    if (fp != NULL) {
        fputs(data, fp);
        fclose(fp);
    } else {
        fprintf(stderr, "Failed to write to file: %s", filepath);
    }
}

int main(int argc, char const *argv[]) {
    char const *proj_name = argv[1];
    if (strcmp(proj_name, "-h") == 0 || strcmp(proj_name, "--help") == 0) {
        puts(help_msg);
        return 0;
    }
    char *makefile_string = NULL;
    if (Sasprintf(makefile_string,
                  "P = %s-exe\n"
                  "OBJECTS = %s.o\n"
                  "CFLAGS = -g -Wall -std=c11  \n"
                  "LDFLAGS = \n"
                  "CC = gcc\n"
                  "all: $(P)\n\n"
                  "$(P): $(OBJECTS)\n"
                  "	$(CC) $(OBJECTS) $(LDFLAGS) -o $(P)\n"
                  "	rm *.o\n"
                  " mkdir -p _bin_target"
                  "	mv $(P) ./_bin_target/\n\n"
                  "$(OBJECTS):\n"
                  "	$(CC) $*.c $(CFLAGS) -c -o $@ \n\n"
                  "clean:\n"
                  "	rm *.o _bin_target/*exe\n\n",
                  proj_name, proj_name) == -1) {
        fprintf(stderr, "Failed to allocate string.\n");
        return 1;
    }
    char *cfile_string = NULL;
    if (Sasprintf(cfile_string,
                  "#define _GNU_SOURCE\n"
                  "#include <stdio.h>\n"
                  "#include <stdlib.h>\n"
                  "#include <string.h>\n"
                  "int main() {\n"
                  "  printf(\"hello\\n\");"
                  "}\n") == -1) {
        fprintf(stderr, "Failed to allocate string.\n");
        return 1;
    }
    regex_error = regcomp(&regex, regex_string, REG_EXTENDED);
    if (regex_error) {
        fprintf(stderr, "could not compile regex");
        return 1;
    }
    regex_error = regexec(&regex, proj_name, 0, NULL, 0);
    if (!regex_error) {
        // directory doesn't exist
        if (stat(proj_name, &st) == -1) {
            MakeDir(proj_name);
            MakeDir2(proj_name, "_bin_target");
            WriteFile(proj_name, "Makefile", makefile_string);
            WriteFileWitExtension(proj_name, "c", cfile_string);
            WriteFile(proj_name, ".gitignore", "**/*exe");
        } else {
            fprintf(stderr, "Directory already exists: %s\n", proj_name);
            return 1;
        }
        return 0;
    } else if (regex_error == REG_NOMATCH) {
        fprintf(stderr, "Illegal project name: %s\n", proj_name);
        return 1;
    } else {
        fprintf(stderr, "Regex match error!\n");
        return 1;
    }
    free(makefile_string);
    free(cfile_string);
}
