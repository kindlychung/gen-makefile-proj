# gen-makefile-proj

Generate a template folder for a makefile project 

## Supported languages

* C, checkout the master branch
* C++, checkout the cpp branch

## Compile

`make`

## Install

There is an install script, it simply copies the binary to `$HOME/opt/bin`. You can do this manually and copy it anywhere in your path. 


tags: c, cpp, c++, linux, unix